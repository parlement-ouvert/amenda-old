const app = require('express')();
const bodyParser = require('body-parser');
const proxy = require('express-http-proxy');
const querystring = require('querystring');
const cookieSession = require('cookie-session');
const nunjucks = require('nunjucks');
const request = require('superagent');
const async = require('async');
const slugify = require('slugify');

nunjucks.configure('./src', {
  autoescape: true,
  express: app,
  noCache: true,
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieSession({
  name: 'session',
  keys: [
    '42', // FIXME: generate actual keys (during provisioning?)
  ],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

app.use((req, res, next) => {
  res.apiResponse = (err, obj, template) => {
    if (!!err) {
      console.log(err);
      res.status(err.status).send(err);
    } else {
      res.send(JSON.parse(nunjucks.render(template, obj)));
    }
  }
  next();
});

app.use((req, res, next) => {
  // The REST API must be stateless. But when we call it from the Web client,
  // we want the session cookie to be enough to authenticate the user.
  // In this middleware, we emulate this by passing the access_token stored in
  // the session cookie as if it was set in the GET query parameters.
  if (!!req.session && !!req.session.access_token) {
    req.query.access_token = req.session.access_token;
  }

  next();
});

function getRequestAuthorizationHeader(req) {
  // If there is an Authorization HTTP header, use it.
  if (!!req.headers['Authorization']) {
    return {'Authorization': req.headers['Authorization']};
  }
  // Else, if we have an access_token in the query, use it.
  // This method is simpler than adding the access_token GET parameter to all
  // queries.
  // https://docs.gitlab.com/ce/api/oauth2.html#access-gitlab-api-with-access-token
  else if (!!req.query.access_token) {
    return {'Authorization': 'Bearer ' + req.query.access_token};
  }
  // Else, if we have a private_token, use it.
  else if (!!req.query.private_token) {
    return {'Private-Token': req.query.private_token};
  }

  return {};
}

/**
 * @api {get} /api/v1/ping Ping
 * @apiVersion 1.0.0
 * @apiDescription Test if the API server is running/responding.
 * @apiName Ping
 * @apiGroup Utils
 *
 * @apiSuccess {String} ping pong
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {"ping":"pong"}
 */
app.use('/api/v1/ping', require('./ping'));

app.get(
  '/api/v1/project',
  (req, res) => request.get(
      'http://localhost:8000/admin/api/v4/'
      + '/projects/'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'project_array.njk'))
);

/**
 * @api {get} /api/v1/project/:project_id/:amendment/articles
 * @apiVersion 1.0.0
 * @apiDescription List all articles for the specified project.
 * @apiName List project articles
 * @apiGroup Article
 *
 * @apiSuccess {String} id ID of the file
 * @apiSuccess {String} name Name of the file
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": "1",
 *         "name": "Article_1.md",
 *       }
 *     ]
 */
app.get(
  '/api/v1/project/:project_id/amendment/:amendment_id/article',
  (req, res) => async.waterfall(
    [
       // fetch the amendment merge request
       (callback) => request.get(
            'http://localhost:8000/admin/api/v4/'
            + '/projects/' + req.params.project_id
            + '/merge_requests/' + req.params.amendment_id
          )
          .set(getRequestAuthorizationHeader(req))
          .end((err, res) => callback(err, res.body)),
      // fetch articles for the corresponding source branch
      (mergeRequest, callback) => request.get(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/repository/tree?ref=' + mergeRequest.source_branch
        )
        .set(getRequestAuthorizationHeader(req))
        .end(callback),
    ],
    (err, gitlabRes) => {
      res.apiResponse(err, gitlabRes, 'tree.njk');
    }
  )
);

/**
 * @api {get} /api/v1/project/:project_id/:amendment/articles
 * @apiVersion 1.0.0
 * @apiDescription List all articles for the specified project/amendment.
 * @apiName List amendment articles
 * @apiGroup Article
 *
 * @apiSuccess {String} id ID of the file
 * @apiSuccess {String} name Name of the file
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": "1",
 *         "name": "Article_1.md"
 *       }
 *     ]
 */
app.get(
  '/api/v1/project/:project_id/article',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/repository/tree?ref=master'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'tree.njk'))
);

/**
 * @api {get} /project/:project_id/:article_id
 * @apiVersion 1.0.0
 * @apiDescription Get an article content for the specified project
 * @apiName Get article content
 * @apiGroup Article
 *
 * @apiSuccess {String} content of the article
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       This is the first article.
 *     ]
 */
app.get(
  '/api/v1/project/:project_id/article/:article_id',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/repository/files/Article_' + req.params.article_id + '.md'
      + '/raw?ref=master'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.status(gitlabRes.status).send(gitlabRes.text))
);

/**
 * @api {get} /project/:project_id/:article_id
 * @apiVersion 1.0.0
 * @apiDescription Get an article content for the specified project/amendment
 * @apiName Get amendment article content
 * @apiGroup Article
 *
 * @apiSuccess {String} content of the article
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       This is the first article.
 *     ]
 */
app.get(
  '/api/v1/project/:project_id/amendment/:amendment_id/article/:article_id',
  (req, res) => async.waterfall(
    [
      // fetch the merge request
      (callback) => request.get(
          'http://localhost:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/merge_requests/' + req.params.amendment_id
        )
        .set(getRequestAuthorizationHeader(req))
        .end((err, res) => callback(err, res.body)),
      // fetch the article file
      (mergeRequest, callback) => request.get(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/repository/files/Article_' + req.params.article_id + '.md'
          + '/raw?ref=' + mergeRequest.source_branch
        )
      .set(getRequestAuthorizationHeader(req))
      .end(callback)
    ],
    (err, gitlabRes) => res.status(gitlabRes.status).send(gitlabRes.text)
  )
);

/**
 * @api {put} /project/:project_id/amendment/:amendment/:article_id
 * @apiVersion 1.0.0
 * @apiDescription Commit the new content of the specified article
 * @apiName Commit article content
 * @apiGroup Article
 *
 * @apiParam (URL) {String} project_id the ID of the project
 * @apiParam (URL) {String} amendment_id the ID of the amendment
 * @apiParam (URL) {String} article_id the ID of the article
 * @apiParam (POST) {String} content the new content of the article
 * @apiParam (POST) {String} commit_message the commit message
 *
 * @apiSuccess {String} file_path path of the modified article
 * @apiSuccess {String} branch the amendment name
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "file_path": "Article_42.md",
 *       "branch": "master"
 *     }
 */
app.put('/api/v1/project/:project_id/amendment/:amendment_id/article/:article_id',
  (req, res) => async.waterfall(
    [
      // fetch the merge request
      (callback) => request.get(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/merge_requests/' + req.params.amendment_id
        )
        .set(getRequestAuthorizationHeader(req))
        .end((err, res) => callback(err, res.body)),
      // FIXME: make sure the MR has the "draft" label
      // commit on the merge request's source branch
      (mergeRequest, callback) => request.put(
        'http://127.0.0.1:8000/admin/api/v4'
        + '/projects/' + req.params.project_id
        + '/repository/files/Article_' + req.params.article_id + '.md'
        + '?branch=' + mergeRequest.source_branch
        // FIXME: why are content and comit_message not used ?
      )
      .set(getRequestAuthorizationHeader(req))
      .end(callback),
    ],
    (err, gitlabRes) => res.status(gitlabRes.status).send(gitlabRes.text)
  )
);

/**
 * @api {post} /api/v1/project/:project_id/amendment
 * @apiVersion 1.0.0
 * @apiDescription Create a new amendment for the specified project.
 * @apiName Create a new amendment
 * @apiGroup Amendment
 *
 * @apiParam (URL) {String} project_id the ID of the project
 * @apiParam (POST) {String} title the title of the amendment
 *
 */
app.post(
  '/api/v1/project/:project_id/amendment',
  (req, res) => async.waterfall(
    [
      (callback) => request.post(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/repository/branches'  
        )
        .send({
          branch: slugify(req.body.title),
          ref: 'master',
        })
        .set(getRequestAuthorizationHeader(req))
        .end((err, gitlabRes) => callback(err, gitlabRes.body.name)),
      (sourceBranch, callback) => request.post(
          'http://127.0.0.1:8000/admin/api/v4'
          + '/projects/' + req.params.project_id
          + '/merge_requests'
        )
        .send({
          id: req.params.project_id,
          title: req.body.title,
          description: req.body.description,
          source_branch: sourceBranch,
          target_branch: 'master',
          labels: 'draft',
        })
        .set(getRequestAuthorizationHeader(req))
        .end(callback),
    ],
    (err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request.njk')
  )
);

/**
 * @api {delete} /api/v1/project/:project_id/amendment/:amendment
 * @apiVersion 1.0.0
 * @apiDescription Delete the specified amendment.
 * @apiName Delete an amendment
 * @apiGroup Amendment
 *
 * @apiParam (URL) {String} project_id the ID of the project
 * @apiParam (URL) {String} amendment_id the amendment id
 * @apiParam (PUT) {Labels} status
 *
 */
app.put(
  '/api/v1/project/:project_id/amendment/:amendment_id',
  (req, res) => request.put(
    'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/merge_requests/' + req.params.amendment_id
  )
  .send({
    // FIXME: check that status is one of draft, accepted...
    labels: req.body.status
  })
  .set(getRequestAuthorizationHeader(req))
  .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request.njk'))
);

/**
 * @api {get} /project/:project_id/amendement/:amendment_id
 * @apiVersion 1.0.0
 * @apiDescription Get an amendment for the specified project.
 * @apiName Get an amendments
 * @apiGroup Amendments
 *
 * @apiSuccess {String} content of the file
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "id": "1",
 *      "name": "first-amendement",
 *      "status": "rejected",
 *         "author": "alice@example.com" 
 *    }
 */
app.get(
  '/api/v1/project/:project_id/amendment/:amendment_id',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/merge_requests/' + req.params.amendment_id
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request.njk'))
);

/**
 * @api {get} /project/:project_id/amendement
 * @apiVersion 1.0.0
 * @apiDescription Get all the amendments for the specified project
 * @apiName Get all amendments
 * @apiGroup Amendments
 *
 * @apiSuccess {String} content of the file
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": "1",
 *         "name": "first-amendement",
 *         "status": "rejected",
 *         "author": "alice@example.com"
 *       },
 *       {
 *         "id": "2",
 *         "name": "second-amendement",
 *         "status": "draft"
 *         "author": "bob@example.com"
 *       }
 *     ]
 */
app.get(
  '/api/v1/project/:project_id/amendment',
  (req, res) => request.get(
      'http://127.0.0.1:8000/admin/api/v4'
      + '/projects/' + req.params.project_id
      + '/merge_requests'
    )
    .set(getRequestAuthorizationHeader(req))
    .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'merge_request_array.njk'))
);

/**
 * @api {get} /user
 * @apiVersion 1.0.0
 * @apiDescription Get the currently authenticated user.
 * @apiName Get user
 * @apiGroup User
 *
 * @apiSuccess {Object} the authenticated user
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Administrator",
 *       "username": "root",
 *       "avatar_url": "https://secure.gravatar.com/avatar/249d3d16937c76c40b67e0a15b802522?s=80&d=identicon",
 *     }
 */
app.get(
  '/api/v1/user',
  (req, res) => request.get(
    'http://localhost:8000/admin/api/v4'
    + '/user/'
  )
  .set(getRequestAuthorizationHeader(req))
  .end((err, gitlabRes) => res.apiResponse(err, gitlabRes, 'user.njk'))
);

app.listen(3001, () => {
  console.log('API server listening on port 3001');
});
