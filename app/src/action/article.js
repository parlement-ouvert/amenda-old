export const REQUEST_ARTICLES = 'REQUEST_ARTICLES';
export const RECEIVE_ARTICLES = 'RECEIVE_ARTICLES';
export const REQUEST_ARTICLE_CONTENT = 'REQUEST_ARTICLE_CONTENT';
export const RECEIVE_ARTICLE_CONTENT = 'RECEIVE_ARTICLE_CONTENT';
export const INVALIDATE_ARTICLE_CONTENT = 'INVALIDATE_ARTICLE_CONTENT';

function requestArticles(project, amendment) {
  return {
    type: REQUEST_ARTICLES,
    project,
    amendment,
  };
}

function receiveArticles(project, amendment, json) {
  return {
    type: RECEIVE_ARTICLES,
    project,
    amendment,
    articles: json,
    receivedAt: Date.now()
  };
}

function fetchArticles(project, amendment) {
  if (!!amendment) {
    return dispatch => {
      dispatch(requestArticles(project, amendment));
      return fetch(`/api/v1/project/${project}/amendment/${amendment}/article`, {credentials: 'same-origin'})
        .then(response => response.json())
        .then(json => dispatch(receiveArticles(project, amendment, json)));
    }
  } else {
    return dispatch => {
      dispatch(requestArticles(project, amendment));
      return fetch(`/api/v1/project/${project}/article`, {credentials: 'same-origin'})
        .then(response => response.json())
        .then(json => dispatch(receiveArticles(project, amendment, json)));
    }
  }
}

function shouldFetchArticles(state, projectId, amendment) {
  if (!state.articlesByProject[projectId] || !state.articlesByProject[projectId][amendment]) {
    return true;
  }

  const articles = state.articlesByProject[projectId][amendment];
  if (!articles) {
    return true;
  } else if (articles.isFetching) {
    return false;
  } else {
    return articles.didInvalidate;
  }
}

export function fetchArticlesIfNeeded(project, amendment = 0) {
  return (dispatch, getState) => {
    if (shouldFetchArticles(getState(), project, amendment)) {
      return dispatch(fetchArticles(project, amendment));
    }
  }
}

export function requestArticleContent(project, amendment, articleId) {
  return {
    type: REQUEST_ARTICLE_CONTENT,
    project,
    amendment,
    articleId,
  };
}

export function invalidateArticleContent(project, amendment, articleId) {
  return {
    type: INVALIDATE_ARTICLE_CONTENT,
    project,
    amendment,
    articleId,
  }
}

export function receiveArticleContent(project, amendment, articleId, content) {
  return {
    type: RECEIVE_ARTICLE_CONTENT,
    project,
    amendment,
    articleId,
    content,
    receivedAt: Date.now()
  };
}

export function fetchArticleContent(project, amendment, articleId) {
  if (!amendment) {
    return (dispatch, getState) => {
      dispatch(requestArticleContent(project, amendment, articleId));
      return fetch(`/api/v1/project/${project}/article/${articleId}`, {credentials: 'same-origin'})
        .then(response => response.text())
        .then(text => dispatch(receiveArticleContent(project, amendment, articleId, text)));
    }
  } else {
    return (dispatch, getState) => {
      dispatch(requestArticleContent(project, amendment, articleId));
      return fetch(`/api/v1/project/${project}/amendment/${amendment}/article/${articleId}`, {credentials: 'same-origin'})
        .then(response => response.text())
        .then(text => dispatch(receiveArticleContent(project, amendment, articleId, text)));
    }
  }
}

function shouldFetchArticleContent(state, project, amendment, articleId) {
  // We assume this action will never be called before the articles for that specific project/amendment
  // have been fetched first.
  const content = state.articleContentById[articleId];

  if (!content) {
    return true;
  } else if (content.isFetching) {
    return false;
  } else {
    return content.didInvalidate;
  }
}

export function fetchArticleContentIfNeeded(project, amendment, articleId) {
  return (dispatch, getState) => {
    if (shouldFetchArticleContent(getState(), project, amendment, articleId)) {
      return dispatch(fetchArticleContent(project, amendment, articleId));
    }
  }
}
