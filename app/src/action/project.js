// import { routerActions } from 'react-router-redux';
import history from '../history';

export const INVALIDATE_PROJECT = 'INVALIDATE_PROJECT';
export const SELECT_PROJECT = 'SELECT_PROJECT';
export const REQUEST_PROJECTS = 'REQUEST_PROJECTS';
export const RECEIVE_PROJECTS = 'RECEIVE_PROJECTS';


function requestProjects() {
  return {
    type: REQUEST_PROJECTS,
  };
}

function receiveProjects(projects) {
  return {
    type: RECEIVE_PROJECTS,
    projects,
    receivedAt: Date.now()
  };
}

function fetchProjects() {
  return dispatch => {
    dispatch(requestProjects());
    return fetch(`/api/v1/project`, {credentials: 'same-origin'})
      .then(response => response.json())
      .then(json => dispatch(receiveProjects(json)));
  }
}

function shouldFetchProjects(state) {
  return !state.projects.items.length && !state.projects.isFetching;
}

export function fetchProjectsIfNeeded(project) {
  return (dispatch, getState) => {
    if (shouldFetchProjects(getState())) {
      return dispatch(fetchProjects());
    }
  }
}

export function selectProject(project) {
  // return (dispatch, getState) => {
    // dispatch(routerActions.push('/project/' + project));
    // history.push('/project/' + project);
    // console.log(action.project);    
    
    return {
      type: SELECT_PROJECT,
      project,
    };
  // }
}

export function invalidateProject(project) {
  return {
    type: INVALIDATE_PROJECT,
    project,
  }
}
