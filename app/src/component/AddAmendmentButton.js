import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import { withStyles } from 'material-ui/styles';

import AddAmendmentDialogContainer from '../container/AddAmendmentDialogContainer';

const styles = theme => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
});

class AddAmendmentButton extends Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render () {
    const { classes } = this.props;
    
    return (
      <div>
        <Button variant="fab" color="primary" aria-label="add" className={classes.fab}
          onClick={()=>this.handleClickOpen()}>
          <AddIcon />
        </Button>
        <AddAmendmentDialogContainer
          open={this.state.open}
          onClose={this.handleClose}
          onSubmit={this.handleClose}/>
      </div>
    );
  }
}

AddAmendmentButton.propTypes = {
  classes: PropTypes.object.isRequired,
  // theme: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddAmendmentButton);
