import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import {
  createAmendment,
} from '../action/amendment';

export default class AddAmendmentDialog extends React.Component {
  state = {
    title: '',
    descrition: '',
  };
  
  submit(e) {
    const { dispatch, project } = this.props;

    dispatch(createAmendment(project, this.state.title, this.state.description));

    if (this.props) {
      this.props.onSubmit(e);
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Créer un nouvel amendement</DialogTitle>
        <DialogContent>
          <DialogContentText>
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Titre"
            value={this.state.title}
            onChange={this.handleChange('title')}
            fullWidth
          />
          <TextField
            margin="dense"
            id="name"
            label="Description"
            value={this.state.description}
            onChange={this.handleChange('description')}
            fullWidth
            multiline
            rows="4"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onClose} color="primary">
            Annuler
          </Button>
          <Button onClick={(e) => this.submit(e)} color="primary">
            Créer
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
