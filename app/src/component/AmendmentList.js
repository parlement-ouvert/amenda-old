import React, { Component } from 'react';
import { List, ListItem } from 'react-toolbox/lib/list';

import { AllHtmlEntities } from 'html-entities';

import AddAmendmentButton from '../component/AddAmendmentButton';

import {
  fetchAmendmentsIfNeeded,
} from '../action/amendment';

export default class AmendmentList extends Component {
  componentDidMount() {
    const { dispatch, project } = this.props;
    
    if (!project) {
      return ;
    }
    
    dispatch(fetchAmendmentsIfNeeded(project));
  }

  componentDidUpdate() {
    const { dispatch, project } = this.props;
    
    if (!project) {
      return ;
    }
    
    dispatch(fetchAmendmentsIfNeeded(project));
  }

  getStatusIcon(status) {
    const icons = {
      draft: 'mode_edit',
      submitted: 'done',
      accepted: 'done_all',
      rejected: 'not_interested',
    };

    return icons[status];
  }
  
  render() {
    if (!this.props.amendments || !this.props.amendments.items) {
      return null;
    }

    return (
      <div>
        <List selectable ripple>
          {
            this.props.amendments.items.map((amendement) =>
              <ListItem
                key={amendement.id}
                caption={AllHtmlEntities.decode(amendement.title)}
                legend={amendement.description}
                leftIcon={this.getStatusIcon(amendement.status)}
                onClick={() => this.props.onAmendmentClick(amendement.id)}
              />
            )
          }
        </List>
        <AddAmendmentButton/>
      </div>
    );
  }
}
