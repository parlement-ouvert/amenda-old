import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'react-toolbox/lib/list';
import Collapsible from 'react-collapsible';
import { connect } from 'react-redux';

import ArticleEditor from './ArticleEditor';

class Article extends Component {
  articleNameToTitle(name) {
    name = name.replace('_', ' ')
      .replace('.md', '');

    return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
  }

  render() {
    return (
      <Collapsible
        trigger={
          <ListItem
            rightIcon="keyboard_arrow_down"
            caption={this.articleNameToTitle(this.props.name)}
            onClick={this.props.onClick}
          />
        }
        triggerWhenOpen={
          <ListItem
            rightIcon="keyboard_arrow_up"
            caption={this.articleNameToTitle(this.props.name)}
            onClick={this.props.onClick}
          />
        }
        easing="ease-in-out">
        <ListItem>
          {
            !!this.props.content && !!this.props.content.text
            ?
            <ArticleEditor
              text={this.props.content ? this.props.content.text : ''}
              readOnly={!this.props.user.id}
            />
            :
            <p>Chargement...</p>
          }
        </ListItem>
      </Collapsible>
    );
  }
}

Article.propTypes = {
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};


function mapStateToProps(state) {
  const { user } = state;

  return {
    user,
  };
}

export default connect(mapStateToProps)(Article);
