import React, { Component } from 'react';
import ContentEditable from 'react-contenteditable'
import ice from 'nytimes-ice';

export default class ArticleEditor extends Component {
  state = {
    readOnly: this.props.readOnly === true,
  };

  enableTracking() {
    if (!!this.tracker) {
      return;
    }

    // const editableElement = this.content.htmlEl.lastChild;
    const editableElement = this.content.htmlEl;
    this.tracker = new ice.InlineChangeEditor({
      // element to track - ice will make it contenteditable
      element: editableElement,
      // tell ice to setup/handle events on the `element`
      handleEvents: true,
      // set a user object to associate with each change
      currentUser: { id: 1, name: 'user' },
      styleColorsNumber: 1,
    });

    // setup and start event handling for track changes
    this.tracker.startTracking();
  }

  disableTracking() {
    if (!this.tracker) {
      return;
    }

    this.tracker.stopTracking();
    this.tracker = null;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      readOnly: nextProps.readOnly === true,
    });
  };

  componentDidMount() {
    if (this.state.readOnly) {
      this.disableTracking();
    } else {
      this.enableTracking();
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.readOnly) {
      this.disableTracking();
    } else {
      this.enableTracking();
    }
  };

  toHTML(text) {
    return text.replace(/\n/g, '<br/>');
  }

  render() {
    if (this.state.readOnly) {
      return (
        <p>{this.props.text}</p>
      );
    } else {
      return (
        <ContentEditable
          ref={(content) => this.content = content}
          html={'<p>' + this.toHTML(this.props.text) + '</p>'}
        />
      );
    }
  }
}
