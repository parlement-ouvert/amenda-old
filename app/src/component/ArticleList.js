import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'react-toolbox/lib/list';

import Article from './Article';

import {
  fetchArticlesIfNeeded,
  fetchArticleContentIfNeeded,
} from '../action/article';

class ArticleList extends Component {
  componentWillMount() {
    const { dispatch, project, selectedAmendment } = this.props;

    if (!project) {
      return ;
    }

    dispatch(fetchArticlesIfNeeded(project, selectedAmendment));
  }

  componentDidUpdate() {
    const { dispatch, project, selectedAmendment } = this.props;

    if (!project) {
      return ;
    }

    dispatch(fetchArticlesIfNeeded(project, selectedAmendment));
  }

  handleArticleClick(article) {
    const { dispatch, project, selectedAmendment } = this.props;

    dispatch(fetchArticleContentIfNeeded(project, selectedAmendment, article.id));
  }

  render() {
    // FIXME: hardcoded ref to "master"
    if (!this.props.project
        || !this.props.articles
        || !this.props.articles[this.props.selectedAmendment]
        || !Array.isArray(this.props.articles[this.props.selectedAmendment].items)) {
      return null;
    }

    return (
      <List selectable ripple>
        {this.props.articles[this.props.selectedAmendment].items.map((article) =>
          <Article
            key={article.id}
            name={article.name}
            content={this.props.articleContentById[article.id]}
            onClick={() => this.handleArticleClick(article)}
          />
        )}
      </List>
    );
  }
}

ArticleList.propTypes = {
  project: PropTypes.number.isRequired,
};

export default ArticleList;
