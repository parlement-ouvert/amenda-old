import React, { Component } from 'react';

import AppBar from 'react-toolbox/lib/app_bar/AppBar';
import Navigation from 'react-toolbox/lib/navigation/Navigation';
import MenuItem from 'react-toolbox/lib/menu/MenuItem';
import Avatar from 'react-toolbox/lib/avatar/Avatar';
import Link from 'react-toolbox/lib/link/Link';

import ButtonMenu from './ButtonMenu';

import {
  fetchUserIfNeeded,
} from '../action/user';

class Header extends Component {

  componentWillMount() {
    this.props.dispatch(fetchUserIfNeeded());
  }

  render() {
    return (
      <AppBar title='Amenda'>
        <Navigation type='horizontal'>
          {!!this.props.user && !!this.props.user.id
            ? <ButtonMenu
                label={this.props.user.name}
                icon={<Avatar style={{height:20,width:20,marginTop:-4}} image={this.props.user.avatar_url}/>}
              >
              <MenuItem
                value='signout'
                icon='power_settings_new'
                caption='Se Déconnecter'
                onClick={(e)=>window.location.href='/oauth/logout?redirect=/'}
              />
            </ButtonMenu>
            : <Link
                href={
                  '/oauth/authorize?response_type=code'
                    + '&redirect_uri=' + process.env.REACT_APP_OAUTH_APP_CALLBACK_URL
                    + '&client_id=' + process.env.REACT_APP_OAUTH_APP_ID
                }
                icon='person_outline'
                label='Se Connecter'
              />}
        </Navigation>
      </AppBar>
    );
  }
}

export default Header;
