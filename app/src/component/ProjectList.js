import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from 'react-toolbox/lib/list';

import history from '../history';

import { routerActions } from 'react-router-redux';

import {
  fetchProjectsIfNeeded,
} from '../action/project';

class ProjectList extends Component {
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(fetchProjectsIfNeeded());
  }

  handleProjectClick(project) {
    const { dispatch } = this.props;

    dispatch(routerActions.push('/project/' + project.id + '/articles'));
  }

  render() {
    if (!this.props.projects) {
      return null;
    }

    return (
      <div>
        <h1>Projets</h1>
        <List selectable ripple>
          {this.props.projects.items.map((project) =>
              <ListItem
                key={project.id}
                caption={project.name}
                onClick={() => this.handleProjectClick(project)}/>
          )}
        </List>
      </div>
    );
  }
}

ProjectList.propTypes = {
    // FIXME
};

export default ProjectList;
