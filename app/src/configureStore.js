import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducer';
import history from './history';
import { routerMiddleware } from 'react-router-redux';

export default function configureStore(preloadedState) {
  return createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(
      thunkMiddleware,
      createLogger(),
      routerMiddleware(history)
    ),
  );
}
