import { connect } from 'react-redux';

import AddAmendmentDialog from '../component/AddAmendmentDialog';

const mapStateToProps = state => {
  return {
    project: state.selectedProject,
  }
};

export default connect(mapStateToProps)(AddAmendmentDialog);
