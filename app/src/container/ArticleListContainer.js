import { connect } from 'react-redux';

import ArticleList from '../component/ArticleList';

const mapStateToProps = state => {
  return {
    project: state.selectedProject,
    articles: state.articlesByProject[state.selectedProject],
    articleContentById: state.articleContentById,
    selectedAmendment: state.selectedAmendment,
  }
};

export default connect(mapStateToProps)(ArticleList);
