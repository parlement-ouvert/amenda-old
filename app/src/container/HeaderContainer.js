import { connect } from 'react-redux';

import Header from '../component/Header';

const mapStateToProps = state => {
  return {
    user: state.user,
  }
};

export default connect(mapStateToProps)(Header);
