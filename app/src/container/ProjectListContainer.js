import { connect } from 'react-redux';

import ProjectList from '../component/ProjectList';

const mapStateToProps = state => {
  return {
    projects: state.projects,
  }
};

export default connect(mapStateToProps)(ProjectList);
