import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

import Layout from 'react-toolbox/lib/layout/Layout';
import Panel from 'react-toolbox/lib/layout/Panel';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';

import './index.css';
import './asset/react-toolbox/theme.css';
import './asset/font/material-icons.css';
import './asset/font/roboto.css';

import theme from './asset/react-toolbox/theme';
import registerServiceWorker from './registerServiceWorker';
import reducers from './reducer';
import Home from './route/Home';
import Project from './route/Project';
import history from './history';
import configureStore from './configureStore';
import HeaderContainer from './container/HeaderContainer';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import purple from 'material-ui/colors/purple';
import green from 'material-ui/colors/green';
import Reboot from 'material-ui/Reboot';

const muiTheme = createMuiTheme({
  palette: {
    primary: {
      light: purple[300],
      main: purple[500],
      dark: purple[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
    },
  },
});

const store = configureStore();

ReactDOM.render(
  <div>
    <Reboot />
    <Provider store={store}>
      <MuiThemeProvider theme={muiTheme}>
        <ThemeProvider theme={theme}>
          <Layout>
            <Panel>
              <HeaderContainer/>
              <div style={{ flex: 1, overflowY: 'auto', padding: '1.8rem' }}>
                <ConnectedRouter history={history}>
                  <div>
                  <Route exact path="/" component={Home}/>
                  <Route exact path="/project/:selectedProject/:tab" component={Project}/>
                  </div>
                </ConnectedRouter>
              </div>
            </Panel>
          </Layout>
        </ThemeProvider>
      </MuiThemeProvider>
    </Provider>
  </div>,
  document.getElementById('root')
);

registerServiceWorker();
