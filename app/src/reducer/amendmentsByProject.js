import {
  INVALIDATE_PROJECT,
  SELECT_PROJECT,
} from '../action/project';

import {
  REQUEST_AMENDMENTS,
  RECEIVE_AMENDMENTS,
} from '../action/amendment';
  
function amendments(
  state = {
    isFetching: false,
    didInvalidate: false,
    items: [],
  },
  action
) {
  switch (action.type) {
    case INVALIDATE_PROJECT:
      return {
        ...state,
        didInvalidate: true,
      }
    case REQUEST_AMENDMENTS:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case RECEIVE_AMENDMENTS:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: action.amendments,
        lastUpdated: action.receivedAt,
      };
    default:
      return state;
  }
}

export default function amendmentsByProject(state = {}, action) {
  switch (action.type) {
    case SELECT_PROJECT:
      return {...state, [action.project]: {}};
    case INVALIDATE_PROJECT:
    case RECEIVE_AMENDMENTS:
    case REQUEST_AMENDMENTS:
      return {
        ...state,
        [action.project]: amendments(state[action.project], action),
      };
    default:
      return state;
  }
}
