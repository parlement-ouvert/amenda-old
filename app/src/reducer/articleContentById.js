import {
  REQUEST_ARTICLE_CONTENT,
  RECEIVE_ARTICLE_CONTENT,
  INVALIDATE_ARTICLE_CONTENT,
} from '../action/article';


function articleContent(
  state = {
    isFetching: false,
    didInvalidate: false,
    text: '',
  },
  action
) {
  switch (action.type) {
    case INVALIDATE_ARTICLE_CONTENT:
      return {
        ...state,
        didInvalidate: true,
      }
    case REQUEST_ARTICLE_CONTENT:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case RECEIVE_ARTICLE_CONTENT:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        text: action.content,
        lastUpdated: action.receivedAt,
      };
    default:
      return state;
  }
}

export default function articleContentById(state = {}, action) {
  switch (action.type) {
    case RECEIVE_ARTICLE_CONTENT:
    case REQUEST_ARTICLE_CONTENT:
    case INVALIDATE_ARTICLE_CONTENT:
      return {
        ...state,
        [action.articleId]: articleContent(state[action.articleId], action)
      };
    default:
      return state;
  }
}
