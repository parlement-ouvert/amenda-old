import {
  INVALIDATE_PROJECT,
  SELECT_PROJECT,
} from '../action/project';

import {
  REQUEST_ARTICLES,
  RECEIVE_ARTICLES,
} from '../action/article';

function articles(
  state = {
    isFetching: false,
    didInvalidate: false,
    items: [],
  },
  action
) {
  switch (action.type) {
    case INVALIDATE_PROJECT:
      return {
        ...state,
        didInvalidate: true,
      }
    case REQUEST_ARTICLES:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case RECEIVE_ARTICLES:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: action.articles,
        lastUpdated: action.receivedAt,
      };
    default:
      return state;
  }
}

export default function articlesByProject(state = {}, action) {
  switch (action.type) {
    case SELECT_PROJECT:
      return {
        ...state,
        [action.project]: {}
      };
    case INVALIDATE_PROJECT:
    case RECEIVE_ARTICLES:
    case REQUEST_ARTICLES:
      return {
        ...state,
        [action.project]: {
          ...state[action.project],
          [action.amendment]: articles(
            state[action.project][action.amendment],
            action
          )
        }
      };
    default:
      return state;
  }
}
