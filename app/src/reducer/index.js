import { combineReducers } from 'redux';
import user from './user';
import projects from './projects';
import selectedProject from './selectedProject';
import articlesByProject from './articlesByProject';
import articleContentById from './articleContentById';
import selectedAmendment from './selectedAmendment';
import amendmentsByProject from './amendmentsByProject';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  router: routerReducer,
  user,
  projects,
  selectedProject,
  articlesByProject,
  articleContentById,
  amendmentsByProject,
  selectedAmendment,
});
