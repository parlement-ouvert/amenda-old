import {
    REQUEST_PROJECTS,
    RECEIVE_PROJECTS,
  } from '../action/project';
  
export default function selectedProject(
    state = {
        isFetching: false,
        didInvalidate: false,
        items: [],
    },
    action
) {
    switch (action.type) {
      case REQUEST_PROJECTS:
        return {
            ...state,
            isFetching: true,
        };
      case RECEIVE_PROJECTS:
        return {
            ...state,
            isFetching: false,
            items: action.projects,
        };
      default:
        return state;
    }
}
