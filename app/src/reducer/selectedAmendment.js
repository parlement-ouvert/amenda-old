import {
  SELECT_AMENDMENT,
} from '../action/amendment';

export default function selectedAmendment(state = 0, action) {
  switch (action.type) {
    case SELECT_AMENDMENT:
      return action.amendment;
    default:
      return state;
  }
}
