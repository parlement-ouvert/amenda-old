import {
  SELECT_PROJECT,
} from '../action/project';

export default function selectedProject(state = 0, action) {
  switch (action.type) {
    case SELECT_PROJECT:
      return action.project;
    default:
      return state;
  }
}
