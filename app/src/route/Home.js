import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ProjectListContainer from '../container/ProjectListContainer';

import {
  selectProject,
} from '../action/project';

class Home extends Component {
  state = {
    index: 0,
    user: null,
  };

  handleTabChange = (index) => {
    this.setState({
      index,
    });
  };

  render() {
    return (
        <ProjectListContainer/>
    );
  }
}

Home.propTypes = {
  selectedProject: PropTypes.number,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  const { selectedProject } = state;

  return {
    selectedProject,
  };
}

export default connect(mapStateToProps)(Home);
