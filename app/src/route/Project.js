import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { routerActions } from 'react-router-redux';

import Tab from 'react-toolbox/lib/tabs/Tab';
import Tabs from 'react-toolbox/lib/tabs/Tabs';

import ArticleListContainer from '../container/ArticleListContainer';
import HeaderContainer from '../container/HeaderContainer';
import AmendmentListContainer from '../container/AmendmentListContainer';

import {
  fetchArticlesIfNeeded,
} from '../action/article';

import {
  selectProject,
} from '../action/project';

class Project extends Component {
  state = {
    index: 0,
    user: null,
  };

  tabs = ['articles', 'amendments'];

  componentWillMount() {
    const { dispatch, match, selectedProject } = this.props;

    if (!!match.params.selectedProject && !selectedProject)
      dispatch(selectProject(parseInt(match.params.selectedProject)));

    this.setTabByName(match.params.tab);
  }

  setTabByName(name) {
    this.setTabByIndex(this.tabs.indexOf(name));
  }

  setTabByIndex(index) {
    this.setState({
      index,
    });
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch, match, selectedProject } = nextProps;

    this.setTabByName(match.params.tab);
  }

  handleTabChange = (index) => {
    this.props.dispatch(routerActions.push(
      '/project/' + this.props.selectedProject + '/' + this.tabs[index]
    ));
  }

  render() {
    return (
      <div>
        <Tabs
          index={this.state.index}
          onChange={this.handleTabChange}>
          <Tab label='Articles'>
              <ArticleListContainer project={this.props.selectedProject}/>
          </Tab>
          <Tab label='Amendements' onActive={this.handleActive}>
              <AmendmentListContainer project={this.props.selectedProject}/>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

Project.propTypes = {
  selectedProject: PropTypes.number,
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  const { selectedProject } = state;

  return {
    selectedProject,
  };
}

export default connect(mapStateToProps)(Project);
