diagram {
  title = "Data flow"

  boundary init {
    title = "Initialisation"
    database legifrance {
      title = "Légifrance"
    }
    function archeolex {
      title = "ArcheoLex"
    }
    io text {
      title = "Texte original"
    }
  }

  boundary edit {
    title = "Edition"
    function edit {
      title = "Modification du texte"
    }
    database git {
      title = "GitLab"
    }
  }

  boundary submit {
    title = "Soumission"
    function duralex {
      title = "DuraLex"
    }
    function sedlex {
      title = "SedLex"
    }
    io diff {
      title = "diff"
    }
    io amendment {
      title = "Amendement"
    }
    io ast {
      title = "AST"
    }
    database eloi {
      title = "ELOI"
    }
    io merge {
      title = "Merge request"
    }
  }

  legifrance -> archeolex {
    operation = "Télécharge l'archive Légifrance"
    data = ["archive.zip"]
  }
  archeolex -> text {
    operation = "Produit un fichier historisé par article"
    data = ["article_1.md", "article_2.md", "article_x.md", "..."]
  }
  text -> edit {
    operation = "Importe le texte original dans l'éditeur"
    data = ["article_x.md"]
  }
  edit -> git {
    operation = "Enregistre la modification"
    data = ["article_x.md"]
  }
  git -> diff {
    operation = "Génère le diff"
    data = ["article_x.md"]
  }
  diff -> duralex {
    operation = "Importe le fichier dans DuraLex"
  }
  duralex -> ast {
    operation = "Transforme le diff en arbre sémantique."
    data = ["article_x.diff"]
  }
  ast -> sedlex {
    operation = "Importe le fichier dans SedLex"
  }
  sedlex -> amendment {
    operation = `Génère le texte de l'amendement
    avec une légistique valide`
    data = ["article_x.json"]
  }
  amendment -> eloi {
    operation = "Soumet l'amendement à l'Assemblée Nationale"
  }
  amendment -> merge {
    operation = "Soumet l'amendement sous forme de merge request"
  }
  merge -> git {
    operation = "test"
  }
}
