const oauthConfig = require('/etc/amenda/oauth.json');
const request = require('superagent');

module.exports = function(req, res) {
  const code = req.query.code;

  request.post('http://127.0.0.1:8000/admin/oauth/token')
    .send({
      client_id: oauthConfig.app_id,
      client_secret: oauthConfig.app_secret,
      code: code,
      grant_type: 'authorization_code',
      redirect_uri: 'https://amenda.test/oauth/callback' // FIXME: read from config
    })
    .then(function(res2) {
      req.session.access_token = res2.body.access_token;
      res.redirect('/');
    })
    .catch(function(err) {
      res.status(500).send({error: err.message});
   });
}
