const app = require('express')();
const proxy = require('express-http-proxy');
const cookieSession = require('cookie-session');

app.use(cookieSession({
  name: 'session',
  keys: [
    '42', // FIXME: generate actual keys (during provisioning?)
  ],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

app.get('/oauth/callback', require('./callback'));
app.get('/oauth/logout', require('./logout'));

app.get('/oauth/authorize', proxy('localhost:8000', {
  proxyReqPathResolver: (req) => {
    return '/admin/oauth/authorize'
      + '?response_type=' + req.query.response_type
      + '&redirect_uri=' + req.query.redirect_uri
      + '&client_id=' + req.query.client_id;
  }
}));

app.listen(3002, () => {
  console.log('OAuth client listening on port 3002');
});
